//
//  FirebaseUserService.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/21/24.
//

import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift

enum FirestoreConstants {
  static let root = Firestore.firestore()
  static let userCollection = root.collection("users")
}

struct FirebaseUserService: UserServiceProtocol {
  func uploadUserData(_ user: UserEntity) async throws -> User {
    do {
      print("DEBUG: [ FirebaseUserService ]  user = \(user))")
      // Transform into Firebase readable format
      let userData = try Firestore.Encoder().encode(user)
      // Upload to Firebase collection
      try await FirestoreConstants.userCollection.document(user.id).setData(userData)

      return user
    } catch {
      throw error
    }
  }

  func fetchUsers() async throws -> [User] {
    let snapshot = try await FirestoreConstants.userCollection.getDocuments()
    let users = snapshot.documents.compactMap { try? $0.data(as: UserEntity.self) }

    return users
  }

  func fetchCurrentUser() async -> User? {
    guard let currentUid = Auth.auth().currentUser?.uid else { return nil }

    do {
      let firestoreUser = FirestoreConstants.userCollection.document(currentUid)
      let currentUser = try await firestoreUser.getDocument(as: UserEntity.self)
      print("DEBUG: [ FirebaseUserService ]  user = \(String(describing: currentUser)))")

      return currentUser
    } catch let DecodingError.dataCorrupted(context) {
      print("DEBUG: [ FirebaseUserService ]  ", context)
    } catch let DecodingError.keyNotFound(key, context) {
      print("DEBUG: [ FirebaseUserService ]  Key '\(key)' not found:", context.debugDescription)
      print("DEBUG: [ FirebaseUserService ]  codingPath:", context.codingPath)
    } catch let DecodingError.valueNotFound(value, context) {
      print("DEBUG: [ FirebaseUserService ]  Value '\(value)' not found:", context.debugDescription)
      print("DEBUG: [ FirebaseUserService ]  codingPath:", context.codingPath)
    } catch let DecodingError.typeMismatch(type, context)  {
      print("DEBUG: [ FirebaseUserService ]  Type '\(type)' mismatch:", context.debugDescription)
      print("DEBUG: [ FirebaseUserService ]  codingPath:", context.codingPath)
    } catch {
      print("DEBUG: [ FirebaseUserService ]  error: ", error)
    }

    return nil
  }
}
