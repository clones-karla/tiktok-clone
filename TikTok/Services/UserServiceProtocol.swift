//
//  UserServiceProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

protocol UserServiceProtocol {
  func uploadUserData(_ user: UserEntity) async throws -> User

  func fetchUsers() async throws -> [User]

  func fetchCurrentUser() async -> User?
}
