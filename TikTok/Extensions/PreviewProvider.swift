//
//  PreviewProvider.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

import Foundation

struct DeveloperPreview {
  static var user = UserModel(
    id: UUID().uuidString,
    username: "karla",
    email: "karla@gmail.com",
    fullname: "Karla Pangilinan"
  )

  static var users: [UserModel] = [
    .init(id: UUID().uuidString, username: "abc", email: "abc@gmail.com", fullname: "Ab C"),
    .init(id: UUID().uuidString, username: "def", email: "def@gmail.com", fullname: "De F"),
    .init(id: UUID().uuidString, username: "ghi", email: "ghi@gmail.com", fullname: "Gh I"),
    .init(id: UUID().uuidString, username: "jkl", email: "jkl@gmail.com", fullname: "Jk L"),
  ]
}
