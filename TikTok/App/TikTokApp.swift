//
//  TikTokApp.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/11/24.
//

import FirebaseCore
import SwiftUI

class AppDelegate: NSObject, UIApplicationDelegate {
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    FirebaseApp.configure()
    return true
  }
}

@main
struct TikTokApp: App {
  // register app delegate for Firebase setup
  @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate

  // Authentication
  @StateObject private var viewModel = ViewModelProvider.shared.makeAuthenticationViewModel()

  var body: some Scene {
    WindowGroup {
      ContentView()
        .onAppear() {
          Task {
            await viewModel.getCurrentUser()
          }
        }
    }
    .environmentObject(viewModel)
  }
}
