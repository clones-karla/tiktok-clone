//
//  Post.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/13/24.
//

import Foundation

struct Post: Identifiable, Codable {
  let id: String
  let videoUrl: String
}
