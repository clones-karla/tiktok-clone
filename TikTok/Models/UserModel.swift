//
//  UserModel.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/21/24.
//

import Foundation

struct UserModel: User, Identifiable {
  var id: String
  var username: String?
  var email: String?
  var fullname: String?
  var bio: String?
  var profileImageUrl: String?
}

extension UserModel: Hashable { }

extension UserModel {
  init(
    username: String? = nil,
    email: String? = nil,
    fullname: String? = nil,
    bio: String? = nil,
    profileImageUrl: String? = nil
  ) {
    self.id = UUID().uuidString
    self.username = username
    self.email = email
    self.fullname = fullname
    self.bio = bio
    self.profileImageUrl = profileImageUrl
  }
}

extension UserModel {
  static func makeUser(from user: User?) -> UserModel? {
    guard let user else { return nil }

    return .init(
      id: user.id,
      username: user.username,
      email: user.email,
      fullname: user.fullname,
      bio: user.bio,
      profileImageUrl: user.profileImageUrl
    )
  }
}

extension UserModel {
  var displayedUsername: String {
    "@\(username ?? "")"
  }

  var displayedFullName: String {
    fullname ?? "?"
  }
}
