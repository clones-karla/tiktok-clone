//
//  MockUserService.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

struct MockUserService: UserServiceProtocol {
  func uploadUserData(_ user: UserEntity) async throws -> User {
    return user
  }
  
  func fetchUsers() async throws -> [User] {
    DeveloperPreview.users
  }

  func fetchCurrentUser() async -> User? {
    DeveloperPreview.user
  }
}
