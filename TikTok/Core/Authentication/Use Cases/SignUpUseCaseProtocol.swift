//
//  SignUpUseCaseProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/19/24.
//

protocol SignUpUseCaseProtocol {
  func execute(
    withUser user: User,
    password: String
  ) async throws -> User
}
