//
//  SignUpUseCase.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/19/24.
//

final class SignUpUseCase: SignUpUseCaseProtocol {
  private let authService: AuthenticationServiceProtocol
  private let userService: UserServiceProtocol

  init(
    authService: AuthenticationServiceProtocol,
    userService: UserServiceProtocol
  ) {
    self.authService = authService
    self.userService = userService
  }

  func execute(
    withUser user: User,
    password: String
  ) async throws -> any User {
    var userEntity = UserEntity.makeUser(from: user)
    let authUser = try await authService.createUser(user, password: password)
    userEntity.id = authUser.id
    let newUser = try await userService.uploadUserData(userEntity)
    print("DEBUG: [ Use Case: SignUp ]  user = \(newUser))")

    return newUser
  }
}
