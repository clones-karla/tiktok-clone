//
//  SignOutUseCaseProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/19/24.
//

protocol SignOutUseCaseProtocol {
  func execute() throws
}
