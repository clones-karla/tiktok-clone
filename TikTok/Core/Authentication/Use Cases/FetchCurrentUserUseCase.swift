//
//  FetchCurrentUserUseCase.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

struct FetchCurrentUserUseCase: FetchCurrentUserUseCaseProtocol {
  private let userService: UserServiceProtocol

  init(userService: UserServiceProtocol) {
    self.userService = userService
  }

  func execute() async -> User? {
    await userService.fetchCurrentUser()
  }
}
