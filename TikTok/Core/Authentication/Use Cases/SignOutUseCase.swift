//
//  SignOutUseCase.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/19/24.
//

final class SignOutUseCase: SignOutUseCaseProtocol {
  private let authService: AuthenticationServiceProtocol

  init(authService: AuthenticationServiceProtocol) {
    self.authService = authService
  }

  func execute() throws {
    try authService.signOut()
  }
}
