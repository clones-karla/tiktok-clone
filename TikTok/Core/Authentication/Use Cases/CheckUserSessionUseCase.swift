//
//  CheckUserSessionUseCase.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/18/24.
//

final class CheckUserSessionUseCase: CheckUserSessionUseCaseProtocol {
  private let authService: AuthenticationServiceProtocol

  init(authService: AuthenticationServiceProtocol) {
    self.authService = authService
  }

  func execute() -> User? {
    return authService.updateUserSession()
  }
}
