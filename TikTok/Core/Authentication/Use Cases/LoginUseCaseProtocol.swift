//
//  LoginUseCaseProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/19/24.
//

protocol LoginUseCaseProtocol {
  func login(
    withEmail email: String,
    password: String
  ) async throws
}
