//
//  CheckUserSessionUseCaseProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/18/24.
//

protocol CheckUserSessionUseCaseProtocol {
  func execute() -> User?
}
