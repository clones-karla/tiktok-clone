//
//  LoginUseCase.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/19/24.
//

final class LoginUseCase: LoginUseCaseProtocol {
  private let authService: AuthenticationServiceProtocol

  init(authService: AuthenticationServiceProtocol) {
    self.authService = authService
  }

  func login(
    withEmail email: String,
    password: String
  ) async throws {
    try? await authService.login(withEmail: email, password: password)
  }
}
