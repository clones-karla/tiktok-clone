//
//  FetchCurrentUserUseCaseProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

protocol FetchCurrentUserUseCaseProtocol {
  func execute() async -> User?
}
