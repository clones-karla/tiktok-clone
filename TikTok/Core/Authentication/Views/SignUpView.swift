//
//  SignUpView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/14/24.
//

import SwiftUI

struct SignUpView: View {
  @State private var email = ""
  @State private var password = ""
  @State private var fullName = ""
  @State private var username = ""
  @Environment(\.dismiss) var dismiss
  @EnvironmentObject var authViewModel: AuthenticationViewModel

  var body: some View {
    VStack {
      Spacer()
      logo
      textFields
      signUpButton
      Spacer()
      signInButton
    }
    .navigationBarBackButtonHidden()
  }
}

extension SignUpView {
  private var logo: some View {
    Image("logo")
      .resizable()
      .scaledToFit()
      .frame(height: 120)
      .padding()
  }

  private var textFields: some View {
    VStack {
      TextField("Enter your email", text: $email)
        .textInputAutocapitalization(.never)
        .modifier(StandardTextFieldModifier())

      SecureField("Enter your password", text: $password)
        .modifier(StandardTextFieldModifier())

      TextField("Enter your full name", text: $fullName)
        .textInputAutocapitalization(.never)
        .modifier(StandardTextFieldModifier())

      TextField("Enter your username", text: $username)
        .textInputAutocapitalization(.never)
        .modifier(StandardTextFieldModifier())
    }
  }

  private var signUpButton: some View {
    Button {
      Task {
        await authViewModel.createUser(
          withEmail: email,
          password: password,
          username: username,
          fullname: fullName
        )
      }
    } label: {
      Text("Sign Up")
        .font(.subheadline)
        .fontWeight(.semibold)
        .foregroundStyle(.white)
        .frame(width: 350, height: 44)
        .background(.pink)
        .cornerRadius(8)
    }
    .padding(.vertical)
    .disabled(!isFormValid)
    .opacity(isFormValid ? 1 : 0.7)
  }

  private var signInButton: some View {
    VStack {
      Divider()
      Button {
        dismiss()
      } label: {
        HStack(spacing: 3) {
          Text("Already have an account?")
          Text("Sign In?")
            .fontWeight(.semibold)
        }
        .font(.footnote)
        .padding(.vertical)
      }
    }
  }
}

// MARK: - AuthenticationFormProtocol

extension SignUpView: AuthenticationFormProtocol {
  var isFormValid: Bool {
    return !email.isEmpty
    && email.contains("@")
    && !password.isEmpty
    && !fullName.isEmpty
    && !username.isEmpty
  }
}


#Preview {
  SignUpView()
}
