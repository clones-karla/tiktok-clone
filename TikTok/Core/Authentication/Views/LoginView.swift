//
//  LoginView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/14/24.
//

import SwiftUI

struct LoginView: View {
  @State private var email = ""
  @State private var password = ""
  @EnvironmentObject var authViewModel: AuthenticationViewModel

  var body: some View {
    NavigationStack {
      VStack {
        Spacer()
        logo
        textFields
        loginButton
        Spacer()
        signUpButton
      }
    }
  }
}

extension LoginView {
  private var logo: some View {
    Image("logo")
      .resizable()
      .scaledToFit()
      .frame(height: 120)
      .padding()
  }

  private var textFields: some View {
    VStack {
      TextField("Enter your email", text: $email)
        .textInputAutocapitalization(.never)
        .modifier(StandardTextFieldModifier())

      SecureField("Enter your password", text: $password)
        .modifier(StandardTextFieldModifier())

      NavigationLink {
        Text("Forgot Password")
      } label: {
        Text("Forgot Password")
          .font(.footnote)
          .fontWeight(.semibold)
          .padding(.top)
          .padding(.trailing, 28)
          .frame(maxWidth: .infinity, alignment: .trailing)
      }
    }
  }

  private var loginButton: some View {
    Button {
      Task {
        await authViewModel.login(withEmail: email, password: password)
      }
    } label: {
      Text("Login")
        .font(.subheadline)
        .fontWeight(.semibold)
        .foregroundStyle(.white)
        .frame(width: 350, height: 44)
        .background(.pink)
        .cornerRadius(8)
    }
    .padding(.vertical)
    .disabled(!isFormValid)
    .opacity(isFormValid ? 1 : 0.7)
  }

  private var signUpButton: some View {
    VStack {
      Divider()
      NavigationLink {
        SignUpView()
      } label: {
        HStack(spacing: 3) {
          Text("Don't have an account?")
          Text("Sign Up?")
            .fontWeight(.semibold)
        }
        .font(.footnote)
        .padding(.vertical)
      }

    }
  }
}

// MARK: - AuthenticationFormProtocol

extension LoginView: AuthenticationFormProtocol {
  var isFormValid: Bool {
    return !email.isEmpty
    && email.contains("@")
    && !password.isEmpty
  }
}


#Preview {
  LoginView()
}
