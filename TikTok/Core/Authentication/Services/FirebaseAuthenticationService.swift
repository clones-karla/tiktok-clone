//
//  FirebaseAuthenticationService.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/16/24.
//

import Firebase
import FirebaseAuth

final class FirebaseAuthenticationService: AuthenticationServiceProtocol {
  private var user: User?

  func updateUserSession() -> User? {
    defer {
      print("DEBUG: [ AuthenticationService ]  Update user session = \(String(describing: user))")
    }

    guard let firebaseUser = Auth.auth().currentUser else {
      user = nil
      return user
    }

    user = UserEntity(id: firebaseUser.uid)
    return user
  }

  func login(
    withEmail email: String,
    password: String
  ) async throws {
    print("DEBUG: [ AuthenticationService ]  login with email \(email)")
    do {
      let result = try await Auth.auth().signIn(withEmail: email, password: password)
      print("DEBUG: [ AuthenticationService ]  User is \(result.user.uid)")
    } catch {
      print("DEBUG: [ AuthenticationService ]  Failed to log in with error: \(error.localizedDescription)")
      throw error
    }
  }

  /// The password must be 6 characters long or more.
  func createUser(_ user: User, password: String) async throws -> any User {
    print("DEBUG: [ AuthenticationService ]  Sign up with email \(String(describing: user.email)) \(String(describing: user.username)) \(String(describing: user.fullname))")
    do {
      let result = try await Auth.auth().createUser(withEmail: user.email ?? "", password: password)
      print("DEBUG: [ AuthenticationService ]  User is \(result.user.uid)")
      let newUser = UserEntity(id: result.user.uid)
      self.user = newUser
      return newUser
    } catch {
      print("DEBUG: [ AuthenticationService ]  Failed to create user with error: \(error.localizedDescription)")
      throw error
    }
  }

  func signOut() throws {
    print("DEBUG: [ AuthenticationService ]  sign out")
    do {
      try Auth.auth().signOut()
      user = nil
    } catch {
      print("DEBUG: [ AuthenticationService ]  Failed to sign out with error: \(error.localizedDescription)")
      throw error
    }
  }
}
