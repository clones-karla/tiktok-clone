//
//  AuthenticationServiceProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/16/24.
//

protocol AuthenticationServiceProtocol {
  func login(
    withEmail email: String,
    password: String
  ) async throws

  func createUser(
    _ user: User,
    password: String
  ) async throws -> User

  func signOut() throws

  func updateUserSession() -> User?
}
