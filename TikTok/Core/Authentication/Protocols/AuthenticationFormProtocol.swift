//
//  AuthenticationFormProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/15/24.
//

import Foundation

protocol AuthenticationFormProtocol {
  var isFormValid: Bool { get }
}
