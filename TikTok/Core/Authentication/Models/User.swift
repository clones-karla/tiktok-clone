//
//  User.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

protocol User {
  var id: String { get set }
  var username: String? { get set }
  var email: String? { get set }
  var fullname: String? { get set }
  var bio: String? { get set }
  var profileImageUrl: String? { get set }
}
