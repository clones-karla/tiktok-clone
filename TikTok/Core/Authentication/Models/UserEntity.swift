//
//  UserEntity.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/18/24.
//

struct UserEntity: User, Identifiable, Codable {
  var id: String
  var username: String?
  var email: String?
  var fullname: String?
  var bio: String?
  var profileImageUrl: String?

  enum CodingKeys: String, CodingKey {
    case id
    case username
    case email
    case fullname = "full_name"
    case bio
    case profileImageUrl = "profile_image_url"
  }
}

extension UserEntity {
  static func makeUser(from user: User) -> UserEntity {
    .init(
      id: user.id,
      username: user.username,
      email: user.email,
      fullname: user.fullname,
      bio: user.bio,
      profileImageUrl: user.profileImageUrl
    )
  }
}
