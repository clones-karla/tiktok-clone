//
//  SignUpViewModel.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/16/24.
//

import Foundation

final class SignUpViewModel: ObservableObject {
  @Published var user: UserModel?

  private let signUpUseCase: SignUpUseCaseProtocol

  init(signUpUseCase: SignUpUseCaseProtocol) {
    self.signUpUseCase = signUpUseCase
  }
}
