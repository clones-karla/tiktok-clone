//
//  LoginViewModel.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/16/24.
//

import Foundation

final class LoginViewModel: ObservableObject {
  private let loginUseCase: LoginUseCaseProtocol

  init(loginUseCase: LoginUseCaseProtocol) {
    self.loginUseCase = loginUseCase
  }

  func login(withEmail email: String, password: String) async {
    do {
      try await loginUseCase.login(withEmail: email, password: password)
    } catch {
      print("DEBUG: [ LoginViewModel ]  Did fail to log in with error: \(error.localizedDescription)")
    }
  }
}
