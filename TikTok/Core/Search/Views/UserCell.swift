//
//  UserCell.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/12/24.
//

import SwiftUI

struct UserCell: View {
  let user: UserModel

  var body: some View {
    HStack(spacing: 12) {
      Image(systemName: "person.circle.fill")
        .resizable()
        .frame(width: 48, height: 48)
        .foregroundStyle(Color(.systemGray6))

      VStack(alignment: .leading) {
        Text(user.displayedUsername)
          .font(.subheadline)
          .fontWeight(.semibold)

        Text(user.displayedFullName)
          .font(.footnote)
      }
      .foregroundStyle(.black)

      Spacer()
    }
  }
}


#Preview {
  UserCell(user: DeveloperPreview.user)
}
