//
//  ExploreView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/12/24.
//

import SwiftUI

struct ExploreView: View {
  @StateObject private var viewModel = ViewModelProvider.shared.makeExploreViewModel()
  @State private var users = DeveloperPreview.users

  var body: some View {
    NavigationStack {
      ScrollView {
        LazyVStack(spacing: 16) {
          ForEach(viewModel.users) { user in
            NavigationLink(value: user) {
              UserCell(user: user)
                .padding(.horizontal)
            }
          }
        }
      }
      .navigationTitle("Explore")
      .navigationDestination(for: UserModel.self) { user in
        UserProfileView(user: user)
      }
      .navigationBarTitleDisplayMode(.inline)
      .padding(.top)
    }
  }
}


#Preview {
  ExploreView()
}
