//
//  FetchUsersUserCase.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

import Foundation

struct FetchUsersUserCase: FetchUsersUserCaseProtocol {
  private let userService: UserServiceProtocol

  init( userService: UserServiceProtocol) {
    self.userService = userService
  }

  func execute() async throws -> [User] {
    try await userService.fetchUsers()
  }
}

