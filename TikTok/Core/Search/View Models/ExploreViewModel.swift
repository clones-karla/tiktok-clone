//
//  ExploreViewModel.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

import Foundation

@MainActor
class ExploreViewModel: ObservableObject {
  @Published var users: [UserModel] = []
  @Published var errorMessage: String = ""

  private let fetchUsersUserCase: FetchUsersUserCaseProtocol

  init(fetchUsersUserCase: FetchUsersUserCaseProtocol) {
    self.fetchUsersUserCase = fetchUsersUserCase

    Task {
      await fetchUsers()
    }
  }
}

extension ExploreViewModel {
  private func fetchUsers() async {
    do {
      users = try await fetchUsersUserCase.execute().compactMap { UserModel.makeUser(from: $0) }
    } catch {
      errorMessage = error.localizedDescription
    }
  }
}

