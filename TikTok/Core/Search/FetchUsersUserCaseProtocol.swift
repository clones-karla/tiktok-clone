//
//  FetchUsersUserCaseProtocol.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

import Foundation

protocol FetchUsersUserCaseProtocol {
  func execute() async throws -> [User]
}
