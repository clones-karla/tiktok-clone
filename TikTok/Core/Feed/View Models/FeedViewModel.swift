//
//  FeedViewModel.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/13/24.
//

import Combine
import Foundation

final class FeedViewModel: ObservableObject {
  @Published var posts = [Post]()

  private let videoUrls = [
    "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",
    "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
    "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
    "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4"
  ]

  private var cancellables: Set<AnyCancellable> = []

  init() {
    fetchPosts()
  }

  deinit {
    cancellables.removeAll()
  }

  func fetchPosts() {
    posts = [
      .init(id: UUID().uuidString, videoUrl: videoUrls[0]),
      .init(id: UUID().uuidString, videoUrl: videoUrls[1]),
      .init(id: UUID().uuidString, videoUrl: videoUrls[2]),
      .init(id: UUID().uuidString, videoUrl: videoUrls[3]),
    ]
  }
}
