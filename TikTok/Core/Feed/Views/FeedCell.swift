//
//  FeedCell.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/12/24.
//

import SwiftUI
import AVKit

struct FeedCell: View {
  let post: Post
  private var player: AVPlayer

  init(post: Post, player: AVPlayer) {
    self.post = post
    self.player = player
  }

  var body: some View {
    ZStack {
      background
      foreground
    }
    .onTapGesture {
      switch player.timeControlStatus {
      case .paused:
        player.play()
      case .waitingToPlayAtSpecifiedRate:
        break
      case .playing:
        player.pause()
      @unknown default:
        break
      }
    }
  }
}

extension FeedCell {
  private var background: some View {
    CustomVideoPlayer(player: player)
      .containerRelativeFrame([.horizontal, .vertical])
  }

  private var foreground: some View {
    VStack {
      Spacer()
      HStack(alignment: .bottom) {
        caption
        Spacer()
        buttons
      }
      .padding(.bottom, 80)
    }
    .padding()
  }

  private var caption: some View {
    VStack (alignment: .leading) {
      Text("karla")
        .fontWeight(.semibold)
      Text("karla pangilinan")
    }
    .foregroundStyle(.white)
    .font(.subheadline)
  }

  private var buttons: some View {
    VStack(spacing: 28) {
      avatar
      likeButton
      commentButton
      bookmarkButton
      shareButton
    }
  }

  private var avatar: some View {
    Circle()
      .frame(width: 48, height: 48)
      .foregroundStyle(.gray)
  }

  private var likeButton: some View {
    Button {

    } label: {
      VStack {
        Image(systemName: "heart.fill")
          .resizable()
          .frame(width: 28, height: 26)
          .foregroundStyle(.white)
        Text("27")
          .font(.caption)
          .foregroundStyle(.white)
          .bold()
      }
    }
  }

  private var commentButton: some View {
    Button {

    } label: {
      VStack {
        Image(systemName: "ellipsis.bubble.fill")
          .resizable()
          .frame(width: 28, height: 26)
          .foregroundStyle(.white)
        Text("27")
          .font(.caption)
          .foregroundStyle(.white)
          .bold()
      }
    }
  }

  private var bookmarkButton: some View {
    Button {

    } label: {
      Image(systemName: "bookmark.fill")
        .resizable()
        .frame(width: 28, height: 26)
        .foregroundStyle(.white)
    }
  }

  private var shareButton: some View {
    Button {

    } label: {
      Image(systemName: "arrowshape.turn.up.right.fill")
        .resizable()
        .frame(width: 28, height: 26)
        .foregroundStyle(.white)
    }
  }
}


#Preview {
  FeedCell(post: Post(id: UUID().uuidString, videoUrl: ""), player: AVPlayer())
}
