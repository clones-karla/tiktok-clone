//
//  FeedView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/12/24.
//

import AVKit
import SwiftUI

struct FeedView: View {
  @StateObject private var viewModel = FeedViewModel()
  @State private var scrollPosition: String?
  @State private var player = AVPlayer() // fixes multiple players playing simultaneously

  var body: some View {
    ScrollView {
      LazyVStack(spacing: 0) {
        ForEach(viewModel.posts) { post in
          FeedCell(post: post, player: player)
            .id(post.id)
            .onAppear {
              playInitialVideoIfNecessary()
            }
        }
      }
      .scrollTargetLayout() // paging effect
    }
    .scrollPosition(id: $scrollPosition)
    .scrollTargetBehavior(.paging) // page locking/snapping effect
    .ignoresSafeArea()
    .onAppear {
      player.play()
    }
    .onDisappear() {
      player.pause()
    }
    .onChange(of: scrollPosition) { oldValue, newValue in
      playerVideo(postId: newValue)
    }
  }
}

extension FeedView {
  private func playerVideo(postId: String?) {
    guard let currentPost = viewModel.posts.first(where: { $0.id == postId }) else {
      return
    }

    player.replaceCurrentItem(with: nil)
    replaceAVPlayerItem(urlString: currentPost.videoUrl)
  }

  private func playInitialVideoIfNecessary() {
    guard
      scrollPosition == nil,
      let post = viewModel.posts.first,
      player.currentItem == nil
    else {
      return
    }

    replaceAVPlayerItem(urlString: post.videoUrl)
  }

  private func replaceAVPlayerItem(urlString: String) {
    guard let url = URL(string: urlString) else {
      print("ERROR: URL is not valid")
      return
    }
    let playerItem = AVPlayerItem(url: url)
    player.replaceCurrentItem(with: playerItem)
    player.play()
  }
}


#Preview {
  FeedView()
}
