//
//  NotificationCell.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/12/24.
//

import SwiftUI

struct NotificationCell: View {
  var body: some View {
    HStack {
      Image(systemName: "person.circle.fill")
        .resizable()
        .frame(width: 32, height: 32)
        .foregroundStyle(Color(.systemGray5))

      HStack {
        Text("karla.pangilinan")
          .font(.footnote)
          .fontWeight(.semibold)

        +

        Text(" liked one of your posts. ")
          .font(.footnote)

        +

        Text("3d")
          .font(.caption)
          .foregroundStyle(.gray)
      }

      Spacer()

      Rectangle()
        .frame(width: 48, height: 48)
        .cornerRadius(6)
    }
    .padding(.horizontal)
  }
}


#Preview {
  NotificationCell()
}
