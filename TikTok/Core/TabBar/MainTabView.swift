//
//  MainTabView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/11/24.
//

import SwiftUI

struct MainTabView: View {
  @EnvironmentObject var authViewModel: AuthenticationViewModel
  @State private var selectedTab: Tab = .feed

  enum Tab: Int {
    case feed, friends, upload, notifications, profile
  }

  var body: some View {
    TabView (selection: $selectedTab) {
      feedTabItem
      friendsTabItem
      uploadTabItem
      inboxTabItem
      profileTabItem
    }
    .tint(.black)
  }
}

extension MainTabView {
  private var feedTabItem: some View {
    FeedView()
      .tabItem {
        VStack {
          Image(systemName: "house")
            .environment(\.symbolVariants, selectedTab == .feed ? .fill : .none)
          Text("Home")
        }
      }
      .onAppear() {
        selectedTab = .feed
      }
      .tag(Tab.feed)
  }

  private var friendsTabItem: some View {
    ExploreView()
      .tabItem {
        VStack {
          Image(systemName: "person.2")
            .environment(\.symbolVariants, selectedTab == .friends ? .fill : .none)
          Text("Friends")
        }
      }
      .onAppear() {
        selectedTab = .friends
      }
      .tag(Tab.friends)
  }

  private var uploadTabItem: some View {
    Text("Upload Post")
      .tabItem {
        VStack {
          Image(systemName: "plus")
        }
      }
      .tag(Tab.upload)
  }

  private var inboxTabItem: some View {
    NotificationsView()
      .tabItem {
        VStack {
          Image(systemName: "heart")
            .environment(\.symbolVariants, selectedTab == .notifications ? .fill : .none)
          Text("Inbox")
        }
      }
      .onAppear() {
        selectedTab = .notifications
      }
      .tag(Tab.notifications)
  }

  private var profileTabItem: some View {
    CurrentUserProfileView()
      .tabItem {
        VStack {
          Image(systemName: "person")
            .environment(\.symbolVariants, selectedTab == .profile ? .fill : .none)
          Text("Profile")
        }
      }
      .onAppear() {
        selectedTab = .profile
      }
      .tag(Tab.profile)
  }
}


#Preview {
  MainTabView()
}
