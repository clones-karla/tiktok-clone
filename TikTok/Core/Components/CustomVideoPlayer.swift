//
//  CustomVideoPlayer.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/13/24.
//

import AVKit
import SwiftUI

struct CustomVideoPlayer: UIViewControllerRepresentable {
  var player: AVPlayer

  func makeUIViewController(context: Context) -> some UIViewController {
    let controller = AVPlayerViewController()
    controller.player = player
    controller.showsPlaybackControls = false
    controller.exitsFullScreenWhenPlaybackEnds = true
    controller.allowsPictureInPicturePlayback = true
    controller.videoGravity = .resizeAspectFill // makes video full screen
    return controller
  }

  func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {

  }
}
