//
//  ViewModelProvider.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/18/24.
//

final class ViewModelProvider {
  static let shared = ViewModelProvider()

  private init() {}

  @MainActor func makeAuthenticationViewModel() -> AuthenticationViewModel {
    let authenticationService = ServiceProvider.shared.makeAuthenticationService()
    let userService = ServiceProvider.shared.makeUserService()

    return AuthenticationViewModel(
      checkSessionUseCase: UseCaseProvider.shared.makeCheckUserSessionUseCase(authService: authenticationService),
      getCurrentUserUseCase: UseCaseProvider.shared.makeGetCurrentUserUseCase(userService: userService),
      signUpUseCase: UseCaseProvider.shared.makeSignUpUseCase(
        authService: authenticationService,
        userService: userService),
      loginUseCase: UseCaseProvider.shared.makeLoginUseCase(authService: authenticationService),
      signOutUseCase: UseCaseProvider.shared.makeSignOutUseCase(authService: authenticationService)
    )
  }

  func makeLoginViewModel() -> LoginViewModel {
    LoginViewModel(loginUseCase: UseCaseProvider.shared.makeLoginUseCase(authService: ServiceProvider.shared.makeAuthenticationService()))
  }

  @MainActor func makeExploreViewModel() -> ExploreViewModel {
    let userService = ServiceProvider.shared.makeUserService()
    return ExploreViewModel(
      fetchUsersUserCase: UseCaseProvider.shared.makeFetchUsersUserCase(userService: userService)
    )
  }
}
