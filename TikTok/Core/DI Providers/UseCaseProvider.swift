//
//  UseCaseProvider.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/18/24.
//

final class UseCaseProvider {
  static let shared = UseCaseProvider()

  private init() {}

  func makeCheckUserSessionUseCase(authService: AuthenticationServiceProtocol) -> CheckUserSessionUseCaseProtocol {
    CheckUserSessionUseCase(authService: authService)
  }

  func makeSignUpUseCase(
    authService: AuthenticationServiceProtocol,
    userService: UserServiceProtocol
  ) -> SignUpUseCaseProtocol {
    SignUpUseCase(
      authService: authService,
      userService: userService
    )
  }

  func makeLoginUseCase(authService: AuthenticationServiceProtocol) -> LoginUseCaseProtocol {
    LoginUseCase(authService: authService)
  }

  func makeSignOutUseCase(authService: AuthenticationServiceProtocol) -> SignOutUseCaseProtocol {
    SignOutUseCase(authService: authService)
  }

  func makeFetchUsersUserCase(userService: UserServiceProtocol) -> FetchUsersUserCaseProtocol {
    FetchUsersUserCase(userService: userService)
  }

  func makeGetCurrentUserUseCase(userService: UserServiceProtocol) -> FetchCurrentUserUseCaseProtocol {
    FetchCurrentUserUseCase(userService: userService)
  }
}
