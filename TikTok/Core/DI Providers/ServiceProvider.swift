//
//  ServiceProvider.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/18/24.
//

final class ServiceProvider {
  static let shared = ServiceProvider()

  private init() {}

  func makeAuthenticationService() -> AuthenticationServiceProtocol {
    FirebaseAuthenticationService()
  }

  func makeUserService() -> UserServiceProtocol {
    FirebaseUserService()
  }
}
