//
//  CurrentUserProfileView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/12/24.
//

import SwiftUI

struct CurrentUserProfileView: View {
  @EnvironmentObject var authViewModel: AuthenticationViewModel

  var body: some View {
    NavigationStack {
      ScrollView {
        VStack(spacing: 2) {
          ProfileHeaderView(user: authViewModel.user ?? DeveloperPreview.user)
          PostGridView()
        }
        .padding(.top)
      }
      .navigationTitle("Profile")
      .navigationBarTitleDisplayMode(.inline)
      .toolbar {
        ToolbarItem(placement: .topBarTrailing) {
          Button("Sign Out") {
            print("DEBUG: Sign out button tapped")
            authViewModel.signOut()
          }
          .font(.subheadline)
          .fontWeight(.semibold)
        }
      }
    }
  }
}


#Preview {
  CurrentUserProfileView()
}
