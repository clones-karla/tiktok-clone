//
//  UserProfileView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/22/24.
//

import SwiftUI

struct UserProfileView: View {
  let user: UserModel

  var body: some View {
    ScrollView {
      VStack(spacing: 2) {
        ProfileHeaderView(user: user)
        PostGridView()
      }
      .padding(.top)
    }
    .navigationTitle("Profile")
    .navigationBarTitleDisplayMode(.inline)
  }
}


#Preview {
  UserProfileView(user: DeveloperPreview.user)
}
