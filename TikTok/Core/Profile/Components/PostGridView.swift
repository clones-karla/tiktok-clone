//
//  PostGridView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/12/24.
//

import SwiftUI

fileprivate enum GridConstant {
  static let numberOfColumns: CGFloat = 3
  static let width = (UIScreen.main.bounds.width / numberOfColumns) - 2
  static let height: CGFloat = 160
  static let rowSpacing: CGFloat = 2
  static let colSpacing: CGFloat = 1
}

struct PostGridView: View {
  // total number of columns
  private let items = [
    GridItem(.flexible(), spacing: GridConstant.colSpacing),
    GridItem(.flexible(), spacing: GridConstant.colSpacing),
    GridItem(.flexible())
  ]

  var body: some View {
    LazyVGrid(columns: items, spacing: GridConstant.rowSpacing) {
      ForEach(0..<15) { post in
        Rectangle()
          .frame(
            width: GridConstant.width,
            height: GridConstant.height
          )
          .clipped()
      }
    }
  }
}


#Preview {
  PostGridView()
}
