//
//  ProfileHeaderView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/12/24.
//

import SwiftUI

struct ProfileHeaderView: View {
  let user: UserModel

  var body: some View {
    VStack(spacing: 16) {
      VStack(spacing: 8) {
        Image(systemName: "person.circle.fill")
          .resizable()
          .frame(width: 80, height: 80)
          .foregroundStyle(Color(.systemGray5))

        Text(user.displayedUsername)
          .font(.subheadline)
          .fontWeight(.semibold)
      }

      stats
      actionButton
      Divider()
    }
  }
}

extension ProfileHeaderView {
  private var stats: some View {
    HStack(spacing: 16) {
      UserStatView(value: 5, title: "Following")
      UserStatView(value: 1, title: "Followers")
      UserStatView(value: 7, title: "Likes")
    }
  }

  private var actionButton: some View {
    Button {

    } label: {
      Text("Edit Profile")
        .font(.subheadline)
        .fontWeight(.semibold)
        .frame(width: 360, height: 32)
        .foregroundStyle(.black)
        .background(Color(.systemGray6))
        .clipShape(RoundedRectangle(cornerRadius: 6))
    }
  }
}


#Preview {
  ProfileHeaderView(user: DeveloperPreview.user)
}
