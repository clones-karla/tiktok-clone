//
//  ContentView.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/11/24.
//

import SwiftUI

struct ContentView: View {
  @EnvironmentObject var authViewModel: AuthenticationViewModel

  var body: some View {
    Group {
      if authViewModel.user != nil {
        MainTabView()
      } else {
        LoginView()
      }
    }
  }
}


#Preview {
  ContentView()
}
