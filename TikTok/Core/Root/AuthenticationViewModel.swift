//
//  AuthenticationViewModel.swift
//  TikTok
//
//  Created by Karla Pangilinan on 6/17/24.
//

import Combine
import FirebaseAuth
import Foundation

@MainActor
class AuthenticationViewModel: ObservableObject {
  @Published var user: UserModel?

  private let checkSessionUseCase: CheckUserSessionUseCaseProtocol
  private let getCurrentUserUseCase: FetchCurrentUserUseCaseProtocol
  private let signUpUseCase: SignUpUseCaseProtocol
  private let loginUseCase: LoginUseCaseProtocol
  private let signOutUseCase: SignOutUseCaseProtocol

  private var cancellables: Set<AnyCancellable> = []

  init(
    checkSessionUseCase: CheckUserSessionUseCaseProtocol,
    getCurrentUserUseCase: FetchCurrentUserUseCaseProtocol,
    signUpUseCase: SignUpUseCaseProtocol,
    loginUseCase: LoginUseCaseProtocol,
    signOutUseCase: SignOutUseCaseProtocol
  ) {
    self.checkSessionUseCase = checkSessionUseCase
    self.getCurrentUserUseCase = getCurrentUserUseCase
    self.signUpUseCase = signUpUseCase
    self.loginUseCase = loginUseCase
    self.signOutUseCase = signOutUseCase

    updateUserSession()
  }

  deinit {
    cancellables.removeAll()
  }

  func updateUserSession() {
    let user = checkSessionUseCase.execute()
    self.user = UserModel.makeUser(from: user)
    print("DEBUG: [ AuthenticationViewModel ]  Update user \(String(describing: self.user))")
  }

  func getCurrentUser() async {
    let user = await getCurrentUserUseCase.execute()
    self.user = UserModel.makeUser(from: user)
    print("DEBUG: [ AuthenticationViewModel ]  Get current user \(String(describing: self.user))")
  }

  func createUser(
    withEmail email: String,
    password: String,
    username: String,
    fullname: String
  ) async {
    do {
      let userModel = UserModel(username: username, email: email, fullname: fullname)
      let user = try await signUpUseCase.execute(withUser: userModel, password: password)
      self.user = UserModel.makeUser(from: user)
    } catch {
      print("DEBUG: [ AuthenticationViewModel ]  Did fail to sign up with error: \(error.localizedDescription)")
      user = nil
    }
  }

  func login(withEmail email: String, password: String) async {
    do {
      print("DEBUG: [ AuthenticationViewModel ]  Login")
      try await loginUseCase.login(withEmail: email, password: password)
      await self.getCurrentUser()
    } catch {
      print("DEBUG: [ AuthenticationViewModel ]  Did fail to log in with error: \(error.localizedDescription)")
    }
  }

  func signOut() {
    do {
      print("DEBUG: [ AuthenticationViewModel ]  Sign out")
      try signOutUseCase.execute()
      self.updateUserSession()
    } catch {
      print("DEBUG: [ AuthenticationViewModel ]  Cannot sign out")
    }
  }
}
